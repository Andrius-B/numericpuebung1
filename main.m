A=[37.5 25 0 12.5 25; 25 0 75 0 0; 0 60 20 0 20; 40 0 0 40 20; 0 25 37.5 0 37.5];
B=[6 8 5 10 9; 3 5 5 2 5; 2 3 3 2 1; 2 3 2 3 2; 4 2 7 3 1; 10 8 13 15 10;...
    10 8 13 15 10; 1 2 3 4 5; 3 1 5 1 3; 13 21 25 31 3; -2 2 -2 2 -2];

% A = [1 2 -1; 2 4 -1; 1 3 1];
% B = [3 5 1];

n=size(A,2);
m=size(B,1);
x=zeros(n,1);
C=zeros(1,m);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
disp('A\b')
for i = 1:m
    b = B(i, :)';
    x=A\b;
end

zeit_g = toc;
disp("Den \-Operator zeit:");
disp(zeit_g)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('lrohne')
%TO-DO Zeitmessung noch Einf�gen
[L1,R1]=lrohne(A);

for i = 1:m
    z=zeros(n, 1);
    x=zeros(n, 1);
    for j=1:n
        %L�se Lz=B(i,:) ohne den Backslash Operator
%         fprintf("bi=%d\n", B(i, j));
%         disp("L * z:");
%         disp("L cut:");
%         disp(L1(j, 1:j));
%         disp("z cut:");
%         disp(z(1:j));
%         disp("Multiplied:");
%         disp(L1(j, 1:j)*(z(1:j)));
%         disp("relevant L elem: ");
%         disp(L1(j,j));
%         fprintf("(%d - %d)/%d=%d\n", B(i, j), L1(j, 1:j)*(z(1:j)), ...
%             L1(j,j), (B(i, j) - L1(j, 1:j)*z(1:j))/L1(j,j));
        z(j)=(B(i, j) - L1(j, 1:j)*z(1:j))/L1(j,j);
    end
%     disp("solved z:");
%     disp(z);
    for j=n:-1:1
%         %TO-DO L�se Rx=z f�r jedes x(j) ohne den Backslash Operator zu nutzen!
%         fprintf("zi=%d\n", z(j));
%         disp("R * x:");
%         disp("R cut:");
%         disp(R1(j, j:n));
%         disp("x cut:");
%         disp(x(j:n));
%         disp("Multiplied:");
%         disp(R1(j, j:n)*x(j:n));
%         disp("relevant L elem: ");
%         disp(R1(j,j));
%         fprintf("(%d - %d)/%d=%d\n", z(j), R1(j, j:n)*x(j:n), ...
%             R1(j,j), (z(j) - R1(j, j:n)*x(j:n))/R1(j,j));
        x(j)=(z(j) - R1(j, j:n)*x(j:n))/R1(j,j);
    end
    disp("Solved x:");
    disp(x);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('lrmit')

%TO-DO Zeitmessung noch Einf�gen

[L2,R2,P]=lrmit(A);
for i = 1:m
    C=P*(B(i, :)'); %Spaltenaenderung der rechten Seite
    z=zeros(n, 1);
    x=zeros(n, 1);
    for j=1:n
        %TO-DO L�se Lz=B(i,:) ohne den Backslash Operator
        z(j)=(C(j) - L2(j, 1:j)*z(1:j))/L2(j,j);
    end
%     disp("Solved z:");
%     disp(z);
    for j=n:-1:1
%         %TO-DO L�se Rx=z f�r jedes x(j) ohne den Backslash Operator zu nutzen!
        x(j)=(z(j) - R2(j, j:n)*x(j:n))/R2(j,j);
    end
    disp("Solved x:");
    disp(x)
    %TO DO L�se x=R\z ohne den Backslash Operator mit einer for-Schleife
end

