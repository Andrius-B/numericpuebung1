function [L,R]= lrohne(A)
n=size(A,1);

for k=1:n-1
%     fprintf("k=%d\n", k);
%     disp("A=");
%     disp(A);
    for i=k+1:n
%         fprintf("i=%d\n", i)
        A(i,k)=A(i,k)/A(k,k);
%         fprintf("Writing l=%d to A(%d, %d)\n", A(i,k), i,k)
%         disp("A=")
%         disp(A);
        for j= k+1:n
%             fprintf("Subtracting: %d - %d*%d = %d\n", A(i,j), A(i,k), A(i,j), A(i,j)-A(i,k)*A(k,j));
            A(i,j) = A(i,j)-A(i,k)*A(k,j); 
        end
    end
end

% disp("Final A=")
% disp(A)

L = tril(A,-1)+eye(n);
% disp("L=");
% disp(L);

R = triu(A,0);
% disp("R=");
% disp(R);

