function [ L,R,P ] = lrmit( A )
%Initialisierung von n,P,L
n=size(A, 1);
P=eye(n);
for k=1:n-1
    %Spaltenpivotsuche
    [~,k0] = max(abs(A(k:n, k)));
    k0 = k0 + k-1;
%     fprintf("At k = %d\n", k);
%     disp("Before step, A=");
%     disp(A);
%     fprintf("The pivot element is in the row: %d\n", k0);
%     disp("max of:")
%     disp(A(k:n, k))
%     fprintf("This will be changed with %d\n", k);

    %Spaltentausch in A
    temp=A(k,:);
    A(k,:)=A(k0,:);
    A(k0,:)=temp;
    
    %Spaltentausch in P
    temp=P(k,:);
    P(k,:)=P(k0,:);
    P(k0,:)=temp;
    
%     disp("After row switch:");
%     disp("A");disp(A);
%     disp("L");disp(L);
%     disp("P");disp(P);
    
    %TO-DO k-ter Eliminationsschritt genau wie in lrohne
    for i=k+1:n
%         fprintf("i=%d\n", i)
        A(i,k)=A(i,k)/A(k,k);
%         fprintf("Writing l=%d to A(%d, %d)\n", A(i,k), i,k)
%         disp("A=")
%         disp(A);
        for j= k+1:n
%             fprintf("Subtracting: %d - %d*%d = %d\n", A(i,j), A(i,k), A(i,j), A(i,j)-A(i,k)*A(k,j));
            A(i,j) = A(i,j)-A(i,k)*A(k,j); 
        end
    end
end
L = tril(A,-1)+eye(n);
R = triu(A,0);
end

